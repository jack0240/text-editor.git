import java.awt.event.*;
import java.awt.*;
//import java.io.*是引入java.io包里的所有类，*是通配符。
import java.io.*;
//导入java swing界面库的类
import javax.swing.*;

class jsb extends JFrame {
    Dialog bb;//对话框
    String strt;
    int i;
    FileDialog fd;
    File file;
    public Frame f;
    public TextArea p1;//文本区域
    public MenuBar menubar;//菜单
    public Menu menu1, menu2, menu3, menu4, menu5;
    public MenuItem item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14;

    jsb(String s)//构造函数
    {
        i = 0;
        f = new Frame(s);
        p1 = new TextArea("");
        f.setBackground(Color.white);//设置窗口背景颜色为白色
        f.setVisible(true);//设置为可见
        menubar = new MenuBar();//设置一个菜单
        menu1 = new Menu("文件");//设置菜单栏
        menu2 = new Menu("编辑");
        menu3 = new Menu("格式");
        menu4 = new Menu("字体");
        menu5 = new Menu("信息");
        item1 = new MenuItem("新建");//具体的菜单条目
        item2 = new MenuItem("打开");
        item3 = new MenuItem("保存");
        item4 = new MenuItem("另存为");
        item5 = new MenuItem("退出");
        item6 = new MenuItem("全选");
        item7 = new MenuItem("复制");
        item8 = new MenuItem("剪切");
        item9 = new MenuItem("粘贴");
        item10 = new MenuItem("字体");
        item11 = new MenuItem("字形");
        item12 = new MenuItem("大小");
        item13 = new MenuItem("颜色");
        item14 = new MenuItem("编程人信息");

        //相应窗口关闭事件
        f.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e) {
                f.setVisible(false);
                System.exit(0);
            }
        });
        menu1.add(item1);//将“新建”写入到文件中
        menu1.add(item2);//将“打开”写入到文件中
        menu1.add(item3);//将“保存”写入到文件中
        menu1.add(item4);//将“另存为”写入到文件中
        menu1.add(item5);//将“退出”写入到文件中
        menu2.add(item6);//将“全选”写入到编辑中
        menu2.add(item7);//将“复制”写入到编辑中
        menu2.add(item8);//将“剪切”写入到编辑中
        menu2.add(item9);//将“粘贴”写入到编辑中
        menu3.add(menu4);//将“字体”写入到格式中
        menu4.add(item10);//将“字体”写入字体到文件中
        menu4.add(item11);//将“字形”写入到字体中
        menu4.add(item12);//将“大小”写入到字体中
        menu4.add(item13);//将“颜色”写入到字体中
        menu5.add(item14);//将“编辑人信息”写入到信息中
        menubar.add(menu1);//文件
        menubar.add(menu2);//格式
        menubar.add(menu3);//字体
        menubar.add(menu5);//信息
        f.setMenuBar(menubar);//设置菜单条
        item1.addActionListener(new Create());//新建
        item2.addActionListener(new OpenFile());//打开
        item3.addActionListener(new SaveFile());//保存
        item4.addActionListener(new SaveAs());//另存为
        item5.addActionListener(new Exit());//退出
        item6.addActionListener(new CheckAll());//全选
        item7.addActionListener(new Copy());//复制
        item8.addActionListener(new Shear());//剪切
        item9.addActionListener(new Paste());//粘贴
        item10.addActionListener(new Font1());//字体
        item11.addActionListener(new Font2());//字形
        item12.addActionListener(new Font3());//大小
        item13.addActionListener(new color());//颜色
        item14.addActionListener(new ProgrammerInfo());//编程人信息
        f.add(p1);
        f.pack();  //放置的组件设定窗口的大小容纳放置的所有组件
    }

    //使用内部类对各个事件进行响应
    class Create implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String ss;
            ss = p1.getText().trim();        //在p1中获得文本中除去空格
            //如果内容不为空就保存
            if (i == 0 && (ss.length() != 0)) {
                bc();
            } else {
                p1.setText("");
                i = 0;//设置文本，如果为空就为空，标志量为0
                f.setTitle("文件对话框");//名字修改为文件对话框
            }
        }
    }

    //打开文件
    class OpenFile implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            fd = new FileDialog(f, "打开文件", 0);//实例话一个文件对话框
            fd.setVisible(true);        //创建并显示打开文件对话框
            try {                        //以缓冲区方式读取文件内容
                file = new File(fd.getDirectory(), fd.getFile());
                f.setTitle(fd.getFile() + "文件对话框");//设置标题为文件对话框
                FileReader fr = new FileReader(file);//文件流的读取
                BufferedReader br = new BufferedReader(fr);//把文件读到缓冲区
                String line = null;//字符串赋值为空
                String view = "";//字符串赋值为空
                //按行读取文本
                while ((line = br.readLine()) != null) {
                    view += line + "\n";//把line里的内容读取到view里
                }
                p1.setText(view);//为view设置文本
                br.close();//关闭文件流
                fr.close();
            } catch (IOException expIn) {
            }
        }
    }

    //保存 文件
    class SaveFile implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (i == 0) {
                bc();
            }
        }
    }

    //另存为
    class SaveAs implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            lbc();
        }
    }

    //退出
    class Exit implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String ss;
            ss = p1.getText().trim();//获得文本中的内容除去空格
            if (i == 0 && (ss.length() != 0))//如果标志量为0且长度不为0
            {
                bc();//就保存
            } else {
                System.exit(0);//否则退出
            }
        }
    }

    //程序员信息
    class ProgrammerInfo implements ActionListener  {
        @Override
        public void actionPerformed(ActionEvent e) {
            bb = new Dialog(f, "编程人信息");//创建对话框，显示相关信息
            Label l1 = new Label("Jack魏 整理");//编程人的信息
            bb.add(l1);//把l1的内容添加到bb下
            bb.setSize(400, 300);//设置窗口大小
            bb.setBackground(Color.white);//设置背景色为白色
            bb.setVisible(true);//把窗口设置为可见
            //窗口的监听器
            bb.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    bb.setVisible(false);//设置为不可见
                    bb.dispose();
                }
            });
        }
    }

    //全选
    class CheckAll implements ActionListener {
        //这是一个事件监听器，可以处理类似单击鼠标时触发的事件 ActionEvent就是一个事件类，传入的e就是该事件的对象
        @Override
        public void actionPerformed(ActionEvent e) {
            p1.setSelectionStart(0);//直接调用系统函数，确定起点
            p1.setSelectionEnd(p1.getText().length());//确定终点
        }
    }

    //复制
    class Copy implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String str = p1.getSelectedText();//获取选择的文本
                //str的长度不为0
                if (str.length() != 0) {
                    strt = str;//str的内容赋值到strt中
                }
            } catch (Exception ex) {
            }        //抛出异常
        }
    }

    //剪切
    class Shear implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String str = p1.getSelectedText();//获取选中的字符
                if (str.length() != 0) {
                    strt = str;
                    p1.replaceRange("", p1.getSelectionStart(), p1.getSelectionEnd());//从开始到末尾剪切，设置为空
                }
            } catch (Exception ex) {
            }
        }
    }

    //粘贴
    class Paste implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (strt.length() > 0) {
                p1.insert(strt, p1.getCaretPosition());//插入选中的字符
            }
        }
    }

    // 字体
    class Font1 extends JPanel implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String str;
            Object[] options = {"黑体", "宋体", "隶书"};//创建一个下拉列表的数组
            Object obj = JOptionPane.showInputDialog(null, "请选择字体类型", "字体", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);//创建一个弹出窗口
            str = obj.toString();//获取返回的字符串
            if (str.equals("黑体"))//当返回的字符串与某字符串相等时，执行相关的if语句
            {
                p1.setFont(new Font("黑体", Font.PLAIN, 16));//设置为黑体
            } else if (str.equals("宋体")) {
                p1.setFont(new Font("宋体", Font.PLAIN, 16));
            } else if (str.equals("隶书")) {
                p1.setFont(new Font("隶书", Font.PLAIN, 16));
            }
        }
    }

    // 字体设置
    class Font2 extends JPanel implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String str;
            String[] options = {"常规", "斜体", "粗体"};//创建一个下拉列表的数组
            Object obj = JOptionPane.showInputDialog(null, "请选择字体形状", "字形", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);//创建一个弹出窗口
            str = obj.toString();//获取返回的字符串
            //当返回的字符串与某字符串相等时，执行相关的if语句
            if (str.equals("常规")) {
                p1.setFont(new Font("宋体", Font.PLAIN, 16));//设置为宋体
            } else if (str.equals("粗体")) {
                p1.setFont(new Font("宋体", Font.BOLD, 16));
            } else if (str.equals("斜体")) {
                p1.setFont(new Font("宋体", Font.ITALIC, 16));
            }
        }
    }

    // 字体大小
    class Font3 extends JPanel implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String str;
            String[] options = {"10", "16", "24"};//创建一个下拉列表的数组
            Object obj = JOptionPane.showInputDialog(null, "请选择字体大小", "大小", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);//创建一个弹出窗口
            str = obj.toString();//获取返回的字符串
            //当返回的字符串与某字符串相等时，执行相关的if语句
            if (str.equals("10")) {
                p1.setFont(new Font("宋体", Font.PLAIN, 10));//设置为10号字体
            } else if (str.equals("16")) {
                p1.setFont(new Font("宋体", Font.PLAIN, 16));
            } else if (str.equals("24")) {
                p1.setFont(new Font("宋体", Font.PLAIN, 24));
            }
        }
    }

    // 字体颜色
    class color extends JPanel implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            //创建颜色对话框接受用户输入的信息
            if (e.getActionCommand() == "颜色") {
                Color newColor = JColorChooser.showDialog(this, "选择颜色", Color.white);//选择颜色
                if (newColor != null)//颜色不为空
                    p1.setForeground(newColor);//设置新的颜色
            }
        }
    }

    public void bc() {
        fd = new FileDialog(f, "保存文件", 1);//打开保存文件对话框
        fd.setVisible(true);//显示为可见
        try {
            file = new File(fd.getDirectory(), fd.getFile());//创建一个文件
            f.setTitle(fd.getFile() + "--记事本");
            FileWriter fw = new FileWriter(file);//文件输入
            BufferedWriter bw = new BufferedWriter(fw);//文件流输入
            String s = p1.getText();
            s = s.replaceAll("\n", "\r\n");//将\n替换\r\n
            bw.write(s);//写入字符串
            bw.flush();//刷新该流的缓冲
            bw.close();//关闭文件流
            fw.close();
            i = 1;
        } catch (IOException expOut) {
        }
    }

    public void lbc() {
        fd = new FileDialog(f, "另存为", 1);
        fd.setVisible(true);
        try {
            file = new File(fd.getDirectory(), fd.getFile());//创建一个文件
            f.setTitle(fd.getFile() + "--记事本");//
            FileWriter fw = new FileWriter(file);//文件输入
            BufferedWriter bw = new BufferedWriter(fw);//文件流输入
            String s = p1.getText();//
            s = s.replaceAll("\n", "\r\n");//将\n替换\r\n
            bw.write(s);//写入字符串
            bw.flush();//刷新该流的缓冲
            bw.close();//关闭文件流
            fw.close();
            i = 1;
        } catch (IOException expOut) {
        }
    }
}

public class TextEditor {
    public static void main(String args[]) {
        jsb dd = new jsb("我的记事本");//创建一个jsb的类对象
    }
}
