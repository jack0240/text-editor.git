# 基于Java Swing的文本编辑器

## 介绍
设计一个类似于Windows记事本(Notepad)的Java程序。可以打开、新建、保存一个文本文件；
对选中的文本进行各种编辑操作（设置字体、字号、字型、对齐方式、背景、前景色、复制、粘贴、剪切等）；
在文本中能够插入对象。


## 相关技术
1.  Java的Swing编程
2.  Java的文件读写


## 所需环境
1.  JDK1.7


## 安装教程
遇到问题可以到**相关博客**进行查看

1.  下载好代码，进入`src`目录

![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/122932_42502d7d_1590078.png "1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/122945_3431c5c6_1590078.png "2.png")

2.  编译
```
javac -encoding UTF-8 TextEditor.java
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123007_a5bab274_1590078.png "3.png")

3.  运行
```
java TextEditor
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123016_43bd1a3a_1590078.png "4.png")
注意：如果修改代码后需要删除所有.class文件，在编译运行！


## 运行截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123023_4dd5ad41_1590078.png "001.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123032_69e996e5_1590078.png "002.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123042_a9ddb35c_1590078.png "003.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123051_914dda8c_1590078.png "004.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/123100_9f33fd2a_1590078.png "005.png")

## 相关博客

1.  [Java指令编译java文件](https://blog.csdn.net/WeiHao0240/article/details/120778832)